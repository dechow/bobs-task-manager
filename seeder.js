const fs = require('fs');
const path = require('path');
const nameGenerator = require('project-name-generator');
const uniqid = require('uniqid');
const slugify = require('slugify');
const dateFns = require('date-fns');
const ms = require('ms');
const ProgressBar = require('progress');

const projects = [];
const hours = [];

function getRandomInt(min = 1, max = 1) {
  return Math.random() * (max - min) + min;
}

function title(str) {
  return str.replace(/(?:^|\s)\w/g, function (match) {
    return match.toUpperCase();
  });
}
const amountOfProjects = getRandomInt(3, 8);

const generateProjects = async (_) => {
  const projectBar = new ProgressBar('Generating Projects [:bar] :percent', {
    complete: '=',
    incomplete: ' ',
    width: 20,
    total: amountOfProjects,
  });
  for (let i = 0; i < amountOfProjects - 1; i++) {
    const projectName = title(nameGenerator().spaced);
    const project = {
      id: uniqid(),
      name: projectName,
      slug: slugify(projectName, { lower: true }),
    };
    projects.push(project);
    projectBar.tick(i + 1);
  }
};

const generateTimeslots = async (_) => {
  for (let i = 0; i < projects.length; i++) {
    const project = projects[i];

    const amountOfTimeSlots = getRandomInt(2, 4);
    const timeBar = new ProgressBar(`Generating Timeslots [:bar] :percent`, {
      complete: '=',
      incomplete: ' ',
      width: 20,
      total: amountOfProjects + amountOfTimeSlots,
    });
    for (let j = 0; j < amountOfTimeSlots - 1; j++) {
      const hourWithin = getRandomInt(1, 3);
      const timeslot = {
        id: uniqid(),
        projectId: project.id,
        started: Date.now() - ms(`${hourWithin} hours`),
        stopped: Date.now(),
        active: false,
        date: dateFns.format(new Date(), 'yyyy/MM/dd'),
      };
      hours.push(timeslot);
      timeBar.tick(j + i);
    }
  }
};
console.log('🐌 Pre-seeding databases \n\n');
generateProjects().then(() => {
  generateTimeslots().then(() => {
    const data = { projects, hours };
    fs.writeFileSync(path.join(__dirname, 'db.json'), JSON.stringify(data));
    console.log('\n\n');
    console.log('✨Done presseded database');
  });
});
