// server.js
const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('db.json');

// const path = require('path')
// const router = jsonServer.router(path.join(__dirname, 'db.json'))

const middlewares = jsonServer.defaults();

server.use(middlewares);
server.use(router);

module.exports = server;
