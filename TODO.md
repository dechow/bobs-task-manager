Bob's Task Manager - Tasks:

- [x] Bob must NOT be able to create projects with the same name twice
- [x] Bob must be able to remove projects
- [x] Bob must be able to start and stop working on projects
- [x] Bob must be able to see an overview of projects and the total time spent on a project in hours
- [x] Changes must be persisted on boot

---

## Nice to have

- [] Bob would like to invoice customers while working on projects, thus creating an invoice with his base price of 500 DKK per hour spent. Creating subsequent invoice for the same project should only contain the hours worked since the last invoice.
- [] Bob would like to mark projects as completed, thus removing them from the overview, they should however always be visible from a project archive view

---

# Specification/Needs

In order to better get an overview of the time he spent on each project he would like a tool where he can register new projects and stop start working on the projects.
He must also be able to see a list of the amount of hours he has spent on each project.
Changes must be persisted between stopping and starting the application.
Choice of frameworks and stack are completely optional but we like working with VueJS and Nuxt.
Upon completing the assignment, you should upload your code to a git repo and share the code with us.
