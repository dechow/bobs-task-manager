import { GetterTree, ActionTree, MutationTree } from 'vuex';
import uid from 'uniqid';
import { format } from 'date-fns';

export interface ITrackedTime {
  id: string;
  projectId: string;
  started: number;
  stopped: number;
  active: boolean;
  date: string;
}

export const state = () => ({
  all: [] as ITrackedTime[],
  active: [] as ITrackedTime[],
});

export type RootState = ReturnType<typeof state>;

const host = '/api';

export const mutations: MutationTree<RootState> = {
  GET_HOURS: (state, fetchedHours: ITrackedTime[]) =>
    (state.all = fetchedHours),
  GET_ACTIVE: (state, fetchedActive: ITrackedTime[]) =>
    (state.active = fetchedActive),
  START_TRACKING: (state, timeSlot: ITrackedTime) => {
    state.active.push(timeSlot);
    state.all.push(timeSlot);
  },
  STOP_TRACKING: (state, timeSlot: ITrackedTime) => {
    let timeslotIndex = state.active.findIndex(
      (p: ITrackedTime) => p.id === timeSlot.id
    );
    state.active[timeslotIndex] = timeSlot;
  },
};

export const actions: ActionTree<RootState, RootState> = {
  async getHours({ commit }) {
    const hours = await this.$axios.$get(`${host}/hours`).then((r) => {
      return r;
    });
    commit('GET_HOURS', hours);
    commit(
      'GET_ACTIVE',
      hours.filter((h: ITrackedTime) => h.active)
    );
  },
  async startTracking({ dispatch, commit }, project: { projectId: string }) {
    const timeSlot: ITrackedTime = {
      id: uid(),
      ...project,
      started: Date.now(),
      stopped: -1,
      active: true,
      date: format(new Date(), 'yyyy/MM/dd'),
    };
    await this.$axios
      .$post(`${host}/hours`, timeSlot)
      .then(() => {
        commit('START_TRACKING', timeSlot);
        dispatch('getHours');
      })
      .catch(() => {
        return;
      });
  },
  async stopTracking(
    { dispatch, commit, state },
    project: { projectId: string }
  ) {
    const currentTimeslot = state.active.find(
      (p: ITrackedTime) => p.projectId === project.projectId
    );
    if (currentTimeslot) {
      const modtimeSlot = {
        ...currentTimeslot,
        stopped: Date.now(),
        active: false,
      };
      await this.$axios
        .$put(`${host}/hours/${modtimeSlot.id}`, modtimeSlot)
        .then(() => {
          commit('STOP_TRACKING', currentTimeslot);
          dispatch('getHours');
        })
        .catch(() => {
          return;
        });
    } else {
      //TODO: Do error handling...
    }
  },
};
