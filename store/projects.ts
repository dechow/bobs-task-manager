import { GetterTree, ActionTree, MutationTree } from 'vuex';
import uid from 'uniqid';
import slugify from 'slugify';
export interface IProject {
  name: string;
  slug: string;
  deletedAt: number;
}

export const state = () => ({
  list: [] as IProject[],
});

export type RootState = ReturnType<typeof state>;

const host = '/api';

export const mutations: MutationTree<RootState> = {
  GET_PROJECTS: (state, fetchedProjects: IProject[]) =>
    (state.list = fetchedProjects),
  ADD_PROJECT: (state, project: IProject) => {
    state.list.push(project);
  },
  REMOVE_PROJECT: (state, project: IProject) => {
    const index = state.list.indexOf(project);
    state.list.splice(index, 1);
  },
};

export const actions: ActionTree<RootState, RootState> = {
  async getProjects({ commit }) {
    const projects = await this.$axios.$get(`${host}/projects`).then((data) => {
      const projectsFiltered = data.filter(
        (project: IProject) => !project.deletedAt
      );
      return projectsFiltered;
    });
    commit('GET_PROJECTS', projects);
  },
  async addProject({ commit, state }, project) {
    const stringNameEquality = (str1: string, str2: string): boolean => {
      const removeSpaces = (str: string): string => str.replace(/\s/g, '');
      return (
        removeSpaces(str1.toLowerCase()) === removeSpaces(str2.toLowerCase())
      );
    };

    const projectExist =
      state.list.filter(
        (listedProject) =>
          stringNameEquality(listedProject.name, project.name) &&
          !listedProject.deletedAt
      ).length > 0;

    if (projectExist) {
      //TODO: Error handling...
      return;
    }
    const createdProject = {
      id: uid(),
      ...project,
      slug: slugify(project.name, { lower: true }),
    };
    await this.$axios
      .$post(`${host}/projects`, createdProject)
      .then(() => {
        commit('ADD_PROJECT', createdProject);
      })
      .catch(() => {
        return;
      });
  },
  async removeProject({ commit }, project) {
    if (!project.id) {
      alert(`Unable to delete ${project.name}`);
    }

    if (window.confirm(`You sure you want to delete: ${project.name}`)) {
      await this.$axios
        .$put(`${host}/projects/${project.id}`, {
          ...project,
          ...{ deletedAt: Date.now() },
        })
        .then(() => {
          commit('REMOVE_PROJECT', project);
        });
    }
  },
};
