# bobs-task-manager

Short assignment for an application.
This was my first dabbling into Nuxt, Vue, Vuex (and their TypeScript implementations).

## Build Setup

```bash
# install dependencies
$ npm install
```

It should install dependencies AND pre-seed the database with projects and time slots.

Then you can run the project with...

```bash
$ npm run dev
```

# To preseed database (after init) run

```bash
$ npm run seed
```
